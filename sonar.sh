#!/bin/bash

      sonar-scanner -X -Dsonar.projectKey=CLAR:Timesheet -Dsonar.sources=. -Dsonar.host.url="$Sonarqube_Server_URL" -Dsonar.login="$Sonarqube_login_token" -Dsonar.gitlab.max_major_issues_gate=0 -Dsonar.qualitygate.wait=true -Dsonar.analysis.mode=publish -Dsonar.qualitygate.timeout=900 -Dsonar.scanner.metadataFilePath='analysis.txt'

      export status=$(cat analysis.txt | jq -r '.task.status') #Status as SUCCESS, CANCELED or FAILED

      export analysisId=$(cat analysis.txt | jq -r '.task.analysisId') #Get the analysis Id

      curl -k -u "$Sonarqube_login_token":"" 3.80.3.64:9002/api/qualitygates/project_status?analysisId=$analysisId -o result.txt; #Analysis result like critical, major and minor issues

      export result=$(cat result.txt | jq -r '.projectStatus.status');

      if [ "$result" == "ERROR" ];then

        echo -e "91mSONAR RESULTS FAILED";

        echo "$(cat result.txt | jq -r '.projectStatus.conditions')"; #prints the critical, major and minor violations

        exit 1 #breaks the build for violations

      else

        echo -e "SONAR RESULTS SUCCESSFUL";

        echo "$(cat result.txt | jq -r '.projectStatus.conditions')";

        exit 0

      fi
